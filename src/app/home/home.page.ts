import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  display: number
  calculo: number
  operacao: string

  constructor() {
    this.display = 0;
    this.calculo = -1;
    this.operacao = "";
  }

  digitaNumero(n: number) {
    if (this.display === this.calculo) {
      this.display = n;
    }
    this.display = parseInt(this.display + `${n}`);
  }

  operacaoMatematica(o: string) {
    this.operacao = o;
    this.calculo = this.display;
  }

  executaCalculo() {
    if (this.operacao === "+") {
      this.display = this.calculo + this.display;
    }
  }

  limparNumeros() {}

}
